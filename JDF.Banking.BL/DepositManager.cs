﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JDF.Banking.BL.Models;
using JDF.Banking.PL;

namespace JDF.Banking.BL
{
    public static class DepositManager
    {
        /**********************************************************************************
        /* Name: Populate
        /* Description: Populates and returns a list of dummy deposits
        /* Parameters:
        /*   int customerId - ID of the customer for which to get deposits
        /* Returns: List of deposits
        /*********************************************************************************/
        public static List<Deposit> Populate(int customerId)
        {
            List<Deposit> deposits = new List<Deposit>();

            switch (customerId)
            {
                case 1:
                    deposits.Add(new Deposit { DepositID = 1, DepositAmount = 2500, DepositDate = new DateTime(2020, 1, 1) });
                    deposits.Add(new Deposit { DepositID = 2, DepositAmount = 300, DepositDate = new DateTime(2020, 1, 15) });
                    deposits.Add(new Deposit { DepositID = 3, DepositAmount = 87, DepositDate = new DateTime(2020, 1, 27) });
                    break;
                case 2:
                    deposits.Add(new Deposit { DepositID = 4, DepositAmount = 15, DepositDate = new DateTime(2017, 6, 8) });
                    deposits.Add(new Deposit { DepositID = 5, DepositAmount = 688, DepositDate = new DateTime(2018, 12, 17) });
                    deposits.Add(new Deposit { DepositID = 6, DepositAmount = 400, DepositDate = new DateTime(2019, 10, 10) });
                    break;
                case 3:
                    deposits.Add(new Deposit { DepositID = 7, DepositAmount = 4816.55, DepositDate = new DateTime(2020, 3, 1) });
                    deposits.Add(new Deposit { DepositID = 8, DepositAmount = 5721.38, DepositDate = new DateTime(2020, 4, 1) });
                    break;
            }

            foreach (Deposit deposit in deposits)
            {
                deposit.CustomerID = customerId;
            }

            return deposits;
        }

        /**********************************************************************************
        /* Name: Load
        /* Description: Loads a list of customer deposits from the database
        /* Parameters:
        /*   int customerId - ID of customer
        /* Returns: List of deposits
        /*********************************************************************************/
        public static List<Deposit> Load(int customerId)
        {
            try
            {
                List<Deposit> deposits = new List<Deposit>();
                Database database = new Database();
                DataTable dataTable = new DataTable();

                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM tblDeposit WHERE CustomerId = @CustomerId");
                sqlCommand.Parameters.AddWithValue("@CustomerId", customerId);

                dataTable = database.Select(sqlCommand);

                foreach (DataRow row in dataTable.Rows)
                {
                    Deposit deposit = new Deposit
                    {
                        DepositID = Convert.ToInt32(row["Id"]),
                        CustomerID = Convert.ToInt32(row["CustomerId"]),
                        DepositDate = Convert.ToDateTime(row["DepositDate"]),
                        DepositAmount = Convert.ToDouble(row["DepositAmount"])
                    };

                    deposits.Add(deposit);
                }

                return deposits;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
