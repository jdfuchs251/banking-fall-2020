﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using JDF.Banking.BL.Models;
using JDF.Banking.PL;

namespace JDF.Banking.BL
{
    public static class CustomerManager
    {
        /**********************************************************************************
        /* Name: Populate
        /* Description: Populates and returns a list of dummy customers
        /* Returns: List of customers
        /*********************************************************************************/
        public static List<Customer> Populate()
        {
            List<Customer> customers = new List<Customer>();

            customers.Add(new Customer
            {
                ID = 1,
                SSN = "445-08-4236",
                FirstName = "Clifford",
                LastName = "Brown",
                BirthDate = new DateTime(1975, 5, 6)
            });

            customers.Add(new Customer 
            { 
                ID = 2,
                SSN = "345-12-7890",
                FirstName = "Miles",
                LastName = "Davis",
                BirthDate = new DateTime(2001, 4, 14)
            });

            customers.Add(new Customer
            {
                ID = 3,
                SSN = "900-01-4888",
                FirstName = "Wynton",
                LastName = "Marsalis",
                BirthDate = new DateTime(1950, 12, 12)
            });

            foreach (Customer c in customers)
            {
                c.Deposits = DepositManager.Populate(c.ID);
                c.Withdrawals = WithdrawalManager.Populate(c.ID);
            }

            return customers;
        }

        /**********************************************************************************
        /* Name: WriteToFile
        /* Description: Writes a list of customers to an XML file
        /* Parameters:
        /*   List<Customers> customers - List of Customer objects to write
        /*********************************************************************************/
        public static void WriteToFile(List<Customer> customers)
        {
            try
            {
                FileIO fileIO = new FileIO();
                fileIO.FileName = Properties.Settings.Default.CustomerXMLFileName;
                fileIO.Delete();

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Customer>));
                TextWriter textWriter = new StreamWriter(fileIO.FileName);

                xmlSerializer.Serialize(textWriter, customers);

                textWriter.Close();
                textWriter = null;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Load
        /* Description: Loads a list of customers from an XML file
        /* Returns: List of customers loaded as List<Customer>
        /*********************************************************************************/
        public static List<Customer> LoadFromFile()
        {
            try
            {
                FileIO fileIO = new FileIO();
                fileIO.FileName = Properties.Settings.Default.CustomerXMLFileName;

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Customer>));
                TextReader textReader = new StreamReader(fileIO.FileName);

                List<Customer> customers = new List<Customer>();
                customers.AddRange((List<Customer>)xmlSerializer.Deserialize(textReader));

                textReader.Close();
                textReader = null;

                return customers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Load
        /* Description: Loads a list of customers from the database
        /* Returns: List of customers loaded as List<Customer>
        /*********************************************************************************/
        public static List<Customer> Load()
        {
            try
            {
                List<Customer> customers = new List<Customer>();
                Database database = new Database();
                DataTable dataTable = new DataTable();

                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM tblCustomer");

                dataTable = database.Select(sqlCommand);

                foreach (DataRow row in dataTable.Rows)
                {
                    Customer customer = new Customer
                    {
                        ID = Convert.ToInt32(row["Id"]),
                        SSN = row["SSN"].ToString(),
                        FirstName = row["FirstName"].ToString(),
                        LastName = row["LastName"].ToString(),
                        BirthDate = Convert.ToDateTime(row["BirthDate"])
                    };

                    customer.Deposits = DepositManager.Load(customer.ID);
                    customer.Withdrawals = WithdrawalManager.Load(customer.ID);

                    customers.Add(customer);
                }

                return customers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Load (1st overload)
        /* Description: Loads a single customer from the database
        /* Parameters:
        /*   int customerId - ID of customer to retrieve
        /* Returns: Customer
        /*********************************************************************************/
        public static Customer Load(int customerId)
        {
            try
            {
                Customer customer = new Customer();
                Database database = new Database();
                DataTable dataTable = new DataTable();

                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM tblCustomer WHERE Id = @Id");
                sqlCommand.Parameters.AddWithValue("@Id", customerId);

                dataTable = database.Select(sqlCommand);
                DataRow row = dataTable.Rows[0];

                customer.ID = Convert.ToInt32(row["Id"]);
                customer.SSN = row["SSN"].ToString();
                customer.FirstName = row["FirstName"].ToString();
                customer.LastName = row["LastName"].ToString();
                customer.BirthDate = Convert.ToDateTime(row["BirthDate"]);
                customer.Deposits = DepositManager.Load(customer.ID);
                customer.Withdrawals = WithdrawalManager.Load(customer.ID);

                return customer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Insert
        /* Description: Inserts a customer into the database
        /* Parameters:
        /*   Customer customer - Customer to be inserted
        /* Returns: True if insert succeeded, false otherwise
        /*********************************************************************************/
        public static bool Insert(Customer customer)
        {
            try
            {
                Database database = new Database();
                SqlCommand sqlCommand = new SqlCommand("INSERT INTO tblCustomer (Id, SSN, FirstName, LastName, BirthDate) " +
                                                       "VALUES (@Id, @SSN, @FirstName, @LastName, @BirthDate)");

                sqlCommand.Parameters.AddWithValue("@Id", customer.ID);
                sqlCommand.Parameters.AddWithValue("@SSN", customer.SSN);
                sqlCommand.Parameters.AddWithValue("@FirstName", customer.FirstName);
                sqlCommand.Parameters.AddWithValue("@LastName", customer.LastName);
                sqlCommand.Parameters.AddWithValue("@BirthDate", customer.BirthDate);

                int rowsInserted = database.Insert(sqlCommand);

                return (rowsInserted == 1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Update
        /* Description: Updates a customer in the database
        /* Parameters:
        /*   Customer customer - Customer to be updated
        /* Returns: True if update succeeded, false otherwise
        /*********************************************************************************/
        public static bool Update(Customer customer)
        {
            try
            {
                Database database = new Database();
                SqlCommand sqlCommand = new SqlCommand("UPDATE tblCustomer " +
                                                       "SET SSN = @SSN, FirstName = @FirstName, LastName = @LastName, BirthDate = @BirthDate " +
                                                       "WHERE Id = @Id");

                sqlCommand.Parameters.AddWithValue("@Id", customer.ID);
                sqlCommand.Parameters.AddWithValue("@SSN", customer.SSN);
                sqlCommand.Parameters.AddWithValue("@FirstName", customer.FirstName);
                sqlCommand.Parameters.AddWithValue("@LastName", customer.LastName);
                sqlCommand.Parameters.AddWithValue("@BirthDate", customer.BirthDate);

                int rowsUpdated = database.Update(sqlCommand);

                return (rowsUpdated == 1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Delete
        /* Description: Deletes a customer from the database
        /* Parameters:
        /*   Customer customer - Customer to be deleted
        /* Returns: True if delete succeeded, false otherwise
        /*********************************************************************************/
        public static bool Delete(Customer customer)
        {
            try
            {
                Database database = new Database();
                SqlCommand sqlCommand = new SqlCommand("DELETE FROM tblCustomer WHERE Id = @Id");

                sqlCommand.Parameters.AddWithValue("@Id", customer.ID);

                int rowsDeleted = database.Delete(sqlCommand);

                return (rowsDeleted == 1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
