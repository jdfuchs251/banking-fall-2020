﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JDF.Banking.BL.Models;
using JDF.Banking.PL;

namespace JDF.Banking.BL
{
    public static class WithdrawalManager
    {
        /**********************************************************************************
        /* Name: Populate
        /* Description: Populates and returns a list of dummy withdrawals
        /* Parameters:
        /*   int customerId - ID of the customer for which to get withdrawals
        /* Returns: List of withdrawals
        /*********************************************************************************/
        public static List<Withdrawal> Populate(int customerId)
        {
            List<Withdrawal> withdrawals = new List<Withdrawal>();

            switch (customerId)
            {
                case 1:
                    withdrawals.Add(new Withdrawal { WithdrawalID = 1, WithdrawalAmount = 1230, WithdrawalDate = new DateTime(2020, 1, 8) });
                    withdrawals.Add(new Withdrawal { WithdrawalID = 2, WithdrawalAmount = 75, WithdrawalDate = new DateTime(2020, 1, 20) });
                    withdrawals.Add(new Withdrawal { WithdrawalID = 3, WithdrawalAmount = 400, WithdrawalDate = new DateTime(2020, 1, 30) });
                    break;
                case 2:
                    withdrawals.Add(new Withdrawal { WithdrawalID = 4, WithdrawalAmount = 520, WithdrawalDate = new DateTime(2019, 3, 2) });
                    withdrawals.Add(new Withdrawal { WithdrawalID = 5, WithdrawalAmount = 945, WithdrawalDate = new DateTime(2019, 4, 11) });
                    break;
                case 3:
                    withdrawals.Add(new Withdrawal { WithdrawalID = 6, WithdrawalAmount = 2001.19, WithdrawalDate = new DateTime(2020, 3, 25) });
                    withdrawals.Add(new Withdrawal { WithdrawalID = 7, WithdrawalAmount = 1472.56, WithdrawalDate = new DateTime(2020, 4, 15) });
                    withdrawals.Add(new Withdrawal { WithdrawalID = 8, WithdrawalAmount = 999, WithdrawalDate = new DateTime(2020, 4, 17) });
                    break;
            }

            foreach (Withdrawal withdrawal in withdrawals)
            {
                withdrawal.CustomerID = customerId;
            }

            return withdrawals;
        }

        /**********************************************************************************
        /* Name: Load
        /* Description: Loads a list of customer withdrawals from the database
        /* Parameters:
        /*   int customerId - ID of customer
        /* Returns: List of withdrawals
        /*********************************************************************************/
        public static List<Withdrawal> Load(int customerId)
        {
            try
            {
                List<Withdrawal> withdrawals = new List<Withdrawal>();
                Database database = new Database();
                DataTable dataTable = new DataTable();

                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM tblWithdrawal WHERE CustomerId = @CustomerId");
                sqlCommand.Parameters.AddWithValue("@CustomerId", customerId);

                dataTable = database.Select(sqlCommand);

                foreach (DataRow row in dataTable.Rows)
                {
                    Withdrawal withdrawal = new Withdrawal
                    {
                        WithdrawalID = Convert.ToInt32(row["Id"]),
                        CustomerID = Convert.ToInt32(row["CustomerId"]),
                        WithdrawalDate = Convert.ToDateTime(row["WithdrawalDate"]),
                        WithdrawalAmount = Convert.ToDouble(row["WithdrawalAmount"])
                    };

                    withdrawals.Add(withdrawal);
                }

                return withdrawals;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
