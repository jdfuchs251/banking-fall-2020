﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using JDF.Banking.PL;
using System.Data;
using System.Data.SqlClient;

namespace JDF.Banking.PL.Test
{
    [TestClass]
    public class utDatabase
    {
        [TestMethod]
        public void OpenTest()
        {
            Database database = new Database();
            Assert.AreEqual(ConnectionState.Open, database.Open());
        }

        [TestMethod]
        public void CloseTest()
        {
            Database database = new Database();
            database.Open();
            Assert.AreEqual(ConnectionState.Closed, database.Close());
        }

        [TestMethod]
        public void SelectTest()
        {
            Database database = new Database();
            SqlCommand sqlCommand = new SqlCommand("SELECT * from tblCustomer");
            DataTable dataTable = database.Select(sqlCommand);
            Assert.AreEqual(3, dataTable.Rows.Count);
        }

        [TestMethod]
        public void InsertTest()
        {
            Database database = new Database();
            SqlCommand sqlCommand = new SqlCommand("INSERT INTO tblCustomer (Id, SSN, FirstName, LastName, BirthDate) " + 
                                                   "VALUES (@Id, @SSN, @FirstName, @LastName, @BirthDate)");
            sqlCommand.Parameters.AddWithValue("@Id", -1);
            sqlCommand.Parameters.AddWithValue("@SSN", "123-12-1234");
            sqlCommand.Parameters.AddWithValue("@FirstName", "John");
            sqlCommand.Parameters.AddWithValue("@LastName", "Smith");
            sqlCommand.Parameters.AddWithValue("@BirthDate", "1/12/1930");

            int rowsInserted = database.Insert(sqlCommand);
            database = null;

            Assert.AreEqual(1, rowsInserted);
        }

        [TestMethod]
        public void UpdateTest()
        {
            Database database = new Database();
            SqlCommand sqlCommand = new SqlCommand("UPDATE tblCustomer SET LastName = @LastName " +
                                                   "Where Id = @Id");
            sqlCommand.Parameters.AddWithValue("@Id", -1);
            sqlCommand.Parameters.AddWithValue("@LastName", "Doe");

            int rowsUpdated = database.Update(sqlCommand);
            database = null;

            Assert.AreEqual(1, rowsUpdated);
        }

        [TestMethod]
        public void DeleteTest()
        {
            Database database = new Database();
            SqlCommand sqlCommand = new SqlCommand("DELETE FROM tblCustomer WHERE Id = @Id");

            sqlCommand.Parameters.AddWithValue("@Id", -1);

            int rowsDeleted = database.Delete(sqlCommand);
            database = null;

            Assert.AreEqual(1, rowsDeleted);
        }
    }
}
