﻿CREATE TABLE [dbo].[tblWithdrawal]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [CustomerId] INT NOT NULL, 
    [WithdrawalDate] DATE NOT NULL, 
    [WithdrawalAmount] MONEY NOT NULL
)
