﻿CREATE TABLE [dbo].[tblDeposit]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [CustomerId] INT NOT NULL, 
    [DepositDate] DATE NOT NULL, 
    [DepositAmount] MONEY NOT NULL
)
