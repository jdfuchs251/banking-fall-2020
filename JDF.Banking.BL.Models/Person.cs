﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JDF.Banking.BL.Models
{
    public class Person
    {
        private string ssn;

        public string SSN
        {
            get { return ssn; }
            set { ssn = value; }
        }

        private string firstName;

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        private string lastName;

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        private DateTime birthDate;

        public DateTime BirthDate
        {
            get { return birthDate; }
            set { birthDate = value; }
        }

        public int Age
        {
            get 
            {
                int years = DateTime.Today.Year - birthDate.Year; // Get rough number of years
                return (birthDate.AddYears(years) > DateTime.Today) ? years - 1 : years; // Subtract a year if person has not had a birthday yet this year
            }
        }

        public string FullName
        {
            get { return firstName + " " + lastName; }
        }
    }
}
