﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JDF.Banking.BL.Models
{
    public class Customer : Person
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private List<Deposit> deposits;

        public List<Deposit> Deposits
        {
            get { return deposits; }
            set { deposits = value; }
        }

        private List<Withdrawal> withdrawals;

        public List<Withdrawal> Withdrawals
        {
            get { return withdrawals; }
            set { withdrawals = value; }
        }

        public double LastDepositAmount
        {
            get { return Deposits.Last().DepositAmount; } // Assume deposits are sorted in chronological order
        }

        public DateTime LastDepositDate
        {
            get { return Deposits.Last().DepositDate; }
        }

        public double LastwithdrawalAmount
        {
            get { return Withdrawals.Last().WithdrawalAmount; } // Assume withdrawals are sorted in chronological order
        }

        public DateTime LastWithdrawalDate
        {
            get { return Withdrawals.Last().WithdrawalDate; }
        }

    }
}
