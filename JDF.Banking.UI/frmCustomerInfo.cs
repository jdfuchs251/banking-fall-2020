﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using JDF.Banking.BL;
using JDF.Banking.BL.Models;

namespace JDF.Banking.UI
{
    public partial class frmCustomerInfo : Form
    {
        List<Customer> customers; // Form-level variable to keep track of list of customers
        const int MSG_DISPLAY_TICKS = 3; // How long a message should display in the status strip
        int msgTickCount;
        bool addCustomerInProgress = false;

        public frmCustomerInfo()
        {
            InitializeComponent();
        }

        /**********************************************************************************
        /* Name: frmCustomerInfo_Load
        /* Description: Gets customer list and binds it to form data grid view
        /*********************************************************************************/
        private void frmCustomerInfo_Load(object sender, EventArgs e)
        {
            try
            {
                // Load customers from static manager
                //customers = CustomerManager.Populate();

                // Load customers from XML file
                //customers = CustomerManager.LoadFromFile();

                // Load customers from database
                customers = CustomerManager.Load();

                BindCustomers();
            }
            catch (Exception ex)
            {
                DisplayMessage(ex.Message, Color.Red);
            }
        }

        /**********************************************************************************
        /* Name: BindCustomers
        /* Description: Binds form-level customer list as data source for dgvCustomers
        /*********************************************************************************/
        private void BindCustomers()
        {
            dgvCustomers.DataSource = null;
            dgvCustomers.DataSource = customers;
            if (dgvCustomers.DataSource != null)
            {
                dgvCustomers.Columns["LastDepositAmount"].Visible = false;
                dgvCustomers.Columns["LastDepositDate"].Visible = false;
                dgvCustomers.Columns["LastWithdrawalAmount"].Visible = false;
                dgvCustomers.Columns["LastWithdrawalDate"].Visible = false;
                dgvCustomers.Columns["FullName"].Visible = false;
            }
        }

        /**********************************************************************************
        /* Name: BindCustomers (1st overload)
        /* Description: Binds form-level customer list as data source for dgvCustomers
        /*   and selects a given index
        /* Parameters:
        /*   int selectionIndex - Index of dgvCustomers to be selected
        /*********************************************************************************/
        private void BindCustomers(int selectionIndex)
        {
            BindCustomers();
            if (selectionIndex >= 0 && selectionIndex < customers.Count) // Make sure index is in range
            {
                dgvCustomers.CurrentCell = dgvCustomers[0, selectionIndex];
            }
            RefreshCustomerSelection();
        }

        /**********************************************************************************
        /* Name: dgvCustomers_SelectionChanged
        /* Description: When a customer is selected, populates form text boxes with customer
        /*              data and clears out deposit and withdrawal data grid views
        /*********************************************************************************/
        private void dgvCustomers_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvCustomers.CurrentRow.Index > -1 && dgvCustomers.CurrentRow.Index < customers.Count)
            {
                RefreshCustomerSelection();

                if (addCustomerInProgress)
                {
                    DisplayMessage("New customer was not saved.", Color.Red);
                    ToggleAddCustomer();
                }
            }
        }

        /**********************************************************************************
        /* Name: RefreshCustomerSelection
        /* Description: Refreshes the text boxes, deposits, and withdrawals for the
        /*   selected customer
        /*********************************************************************************/
        private void RefreshCustomerSelection()
        {
            // Unbind deposit and withdrawal data grid views
            dgvDeposits.DataSource = null;
            dgvWithdrawals.DataSource = null;

            if (dgvCustomers.CurrentRow == null) // No customer selected - clear text boxes
            {
                ClearTextBoxes();
            }
            else
            {
                // Get selected customer
                Customer customer = customers[dgvCustomers.CurrentRow.Index];

                // Populate text boxes
                txtId.Text = customer.ID.ToString();
                txtFirstName.Text = customer.FirstName;
                txtLastName.Text = customer.LastName;
                txtSSN.Text = customer.SSN;
                txtBirthDate.Text = customer.BirthDate.ToString("d");
                txtAge.Text = customer.Age.ToString();

                // Bind deposits
                dgvDeposits.DataSource = customer.Deposits;
                if (dgvDeposits.DataSource != null)
                {
                    dgvDeposits.Columns["DepositAmount"].DefaultCellStyle.Format = "c";
                    dgvDeposits.Columns["DepositAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvDeposits.Columns["CustomerID"].Visible = false;
                }

                // Bind withdrawals
                dgvWithdrawals.DataSource = customer.Withdrawals;
                if (dgvWithdrawals.DataSource != null)
                {
                    dgvWithdrawals.Columns["WithdrawalAmount"].DefaultCellStyle.Format = "c";
                    dgvWithdrawals.Columns["WithdrawalAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvWithdrawals.Columns["CustomerID"].Visible = false;
                }
            }
        }

        /**********************************************************************************
        /* Name: ClearTextBoxes
        /* Description: Clears out all text boxes on the screen
        /*********************************************************************************/
        private void ClearTextBoxes()
        {
            txtId.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtSSN.Text = string.Empty;
            txtBirthDate.Text = string.Empty;
            txtAge.Text = string.Empty;
        }

        /**********************************************************************************
        /* Name: btnNew_Click
        /* Description: Adds a new customer to dgvCustomers
        /*********************************************************************************/
        private void btnNew_Click(object sender, EventArgs e)
        {
            if (!addCustomerInProgress) // Prepare form to add the customer
            {
                ClearTextBoxes();
                txtSSN.Focus();
                dgvDeposits.DataSource = null;
                dgvWithdrawals.DataSource = null;
                ToggleAddCustomer();
            }
            else if (ValidateCustomerFields()) // Add the new customer if validation checks pass
            {
                try
                {
                    Customer customer = new Customer();

                    // Set ID
                    if (customers.Count == 0) customer.ID = 1; // No customers exist - start at 1
                    else customer.ID = customers.Max(c => c.ID) + 1; // Otherwise, get next available ID

                    // Get other fields from form
                    customer.FirstName = txtFirstName.Text.Trim();
                    customer.LastName = txtLastName.Text.Trim();
                    customer.SSN = txtSSN.Text;
                    customer.BirthDate = DateTime.Parse(txtBirthDate.Text);
                    customer.Deposits = new List<Deposit>();  // These two lines are here so the deposit and withdrawal properties are not null
                    customer.Withdrawals = new List<Withdrawal>();

                    // Add customer
                    if (CustomerManager.Insert(customer)) // Add customer to the database
                    {
                        customers.Add(customer); // Add customer to generic list

                        DisplayMessage("Added new customer.", Color.Blue);
                        ToggleAddCustomer();

                        BindCustomers(customers.Count - 1); // Select the customer that was just added
                    }
                }
                catch (Exception ex)
                {
                    DisplayMessage(ex.Message, Color.Red);
                    ToggleAddCustomer();
                }
            }
        }

        /**********************************************************************************
        /* Name: ToggleAddCustomer
        /* Description: Toggles form between its default state and "add customer" mode
        /*********************************************************************************/
        private void ToggleAddCustomer()
        {
            if (!addCustomerInProgress)
            {
                btnNew.Text = "Add";
                btnUpdate.Text = "Cancel";
                btnDelete.Enabled = false;
                addCustomerInProgress = true;
            }
            else
            {
                btnNew.Text = "New";
                btnUpdate.Text = "Update";
                btnDelete.Enabled = true;
                addCustomerInProgress = false;
            }
        }

        /**********************************************************************************
        /* Name: btnUpdate_Click
        /* Description: Saves customer data on form to local generic list
        /*********************************************************************************/
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (addCustomerInProgress) // Update button is functioning as a "Cancel" button
            {
                DisplayMessage("New customer was not saved.", Color.Red);
                ToggleAddCustomer();
                RefreshCustomerSelection();
                return;
            }

            if (customers.Count == 0) return; // No customer to update

            if (ValidateCustomerFields()) // Proceed with update if validation checks pass
            {
                try
                {
                    Customer customer = customers[dgvCustomers.CurrentRow.Index];

                    customer.FirstName = txtFirstName.Text.Trim();
                    customer.LastName = txtLastName.Text.Trim();
                    customer.SSN = txtSSN.Text;
                    customer.BirthDate = DateTime.Parse(txtBirthDate.Text);

                    if (CustomerManager.Update(customer)) // Update customer in the database
                    {
                        BindCustomers(); // Rebind screen if database update succeeded
                        DisplayMessage("Updated customer data.", Color.Blue);
                    }
                }
                catch (Exception ex)
                {
                    DisplayMessage(ex.Message, Color.Red);
                    
                    // If update failed, try to revert generic list back to match the database to keep them in sync
                    try
                    {
                        int index = dgvCustomers.CurrentRow.Index;
                        customers[index] = CustomerManager.Load(customers[index].ID);
                        RefreshCustomerSelection();
                    }
                    catch (Exception)
                    {
                        DisplayMessage("An unexpected error occurred. Customer data was not saved to database.", Color.Red);
                    }
                }
            }
        }

        /**********************************************************************************
        /* Name: ValidateCustomerFields
        /* Description: Validates text box data on form
        /* Returns: True if all fields contain valid data. False otherwise.
        /*********************************************************************************/
        private bool ValidateCustomerFields()
        {
            if (!ValidateSSN()) return false;
            if (!ValidateName(txtFirstName, "First name")) return false;
            if (!ValidateName(txtLastName, "Last name")) return false;
            if (!ValidateBirthDate()) return false;

            return true;
        }

        /**********************************************************************************
        /* Name: ValidateName
        /* Description: Validates a name text box on form. Name must not be empty, must
        /*   not contain only spaces, and must not contain numbers or special characters.
        /* Parameters:
        /*   TextBox nameTextBox - Form text box to be validated
        /*        string caption - Field caption to use in error messages
        /* Returns: True if field contains valid data. False otherwise.
        /*********************************************************************************/
        private bool ValidateName(TextBox nameTextBox, string caption)
        {
            bool isValid = false;
            string errorText = string.Empty;

            if (nameTextBox.Text == string.Empty) errorText = caption + " cannot be empty.";
            else if (nameTextBox.Text.Trim() == string.Empty) errorText = caption + " must contain text.";
            else if (!Regex.IsMatch(nameTextBox.Text, @"^[A-Za-z\s.]*$")) errorText = caption + " may not contain numbers or special characters.";
            else isValid = true;

            if (!isValid)
            {
                DisplayMessage(errorText, Color.Red);
                SelectAndFocusTextBox(nameTextBox);
            }

            return isValid;
        }

        /**********************************************************************************
        /* Name: ValidateSSN
        /* Description: Validates SSN from text box. SSN must be in format 123-45-6789.
        /* Returns: True if field contains valid data. False otherwise.
        /*********************************************************************************/
        private bool ValidateSSN()
        {
            if (!Regex.IsMatch(txtSSN.Text, @"^\d{3}-\d{2}-\d{4}$"))
            {
                DisplayMessage("SSN must match format 123-45-6789.", Color.Red);
                SelectAndFocusTextBox(txtSSN);
                return false;
            }

            return true;
        }

        /**********************************************************************************
        /* Name: ValidateBirthDate
        /* Description: Validates birth date from text box. Birth date must be parse-able
        /*   and must not be in the future.
        /* Returns: True if field contains valid data. False otherwise.
        /*********************************************************************************/
        private bool ValidateBirthDate()
        {
            bool isValid = false;
            string errorText = string.Empty;

            if (!DateTime.TryParse(txtBirthDate.Text, out DateTime birthDate)) errorText = "Birth date format is invalid.";
            else if (birthDate.CompareTo(DateTime.Now) > 0) errorText = "Birth date may not be in the future.";
            else isValid = true;

            if (!isValid)
            {
                DisplayMessage(errorText, Color.Red);
                SelectAndFocusTextBox(txtBirthDate);
            }

            return isValid;
        }

        /**********************************************************************************
        /* Name: SelectAndFocusTextBox
        /* Description: Selects all text and puts cursor into a text box
        /* Parameters:
        /*   TextBox textBox - Which text box to select
        /*********************************************************************************/
        private void SelectAndFocusTextBox(TextBox textBox)
        {
            textBox.SelectAll();
            textBox.Focus();
        }

        /**********************************************************************************
        /* Name: EnterTextBox
        /* Description: Select all text when a user tabs into a text box
        /*********************************************************************************/
        private void EnterTextBox(object sender, EventArgs e)
        {
            SelectAndFocusTextBox((TextBox)sender);
        }

        /**********************************************************************************
        /* Name: btnDelete_Click
        /* Description: Deletes the selected customer from dgvCustomers
        /*********************************************************************************/
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (customers.Count == 0) return; // No customers available to delete

            // Get the customer
            int index = dgvCustomers.CurrentRow.Index; // Hold index for reuse
            Customer customer = customers[index];

            // Prompt to confirm delete
            string dialogPrompt = "Delete customer " + customer.ID.ToString() + ": " + customer.FullName + "?";
            DialogResult dialogResult = MessageBox.Show(dialogPrompt, "Confirm delete", MessageBoxButtons.YesNo);

            // Do the delete
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    if (CustomerManager.Delete(customer)) // Delete customer from the database
                    {
                        customers.Remove(customer); // If database delete succeeded, delete customer from generic list
                        if (index == customers.Count) index--; // Update index if deleting the last customer in the list
                        BindCustomers(index); // Rebind

                        DisplayMessage("Customer deleted.", Color.Blue); 
                    }
                }
                catch (Exception ex)
                {
                    DisplayMessage(ex.Message, Color.Red);
                }
            }
        }

        /**********************************************************************************
        /* Name: DisplayMessage
        /* Description: Displays a message in the form status strip
        /* Parameters:
        /*   string messageText - Message to display
        /*   Color messageColor - Message color
        /*********************************************************************************/
        private void DisplayMessage(string messageText, Color messageColor)
        {
            msgTickCount = 0;
            lblStatus.Text = messageText;
            lblStatus.ForeColor = messageColor;
            timer.Start();
        }

        /**********************************************************************************
        /* Name: timer_Tick
        /* Description: Used to track how long a message appears in the form status strip.
        /*   When tick count reaches MSG_DISPLAY_TICKS, the label is cleared and the timer
        /*   is stopped.
        /*********************************************************************************/
        private void timer_Tick(object sender, EventArgs e)
        {
            msgTickCount++;
            if (msgTickCount == MSG_DISPLAY_TICKS)
            {
                lblStatus.Text = string.Empty;
                timer.Stop();
            }
        }
    }
}
