﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.Banking.BL;
using JDF.Banking.BL.Models;
using System.Collections.Generic;

namespace JDF.Banking.BL.Test
{
    [TestClass]
    public class utCustomer
    {
        [TestMethod]
        public void PopulateTest()
        {
            List<Customer> customers = CustomerManager.Populate();
            Assert.AreEqual(3, customers.Count);
            Assert.IsTrue(customers[0].Deposits.Count > 0);
        }

        [TestMethod]
        public void LoadTest()
        {
            List<Customer> customers = CustomerManager.Load();
            Assert.AreEqual(3, customers.Count);
        }

        [TestMethod]
        public void LoadCustomerTest()
        {
            Customer customer = CustomerManager.Load(1);
            Assert.AreEqual(3, customer.Deposits.Count);
        }

        [TestMethod]
        public void InsertTest()
        {
            Customer customer = new Customer
            {
                ID = -1,
                SSN = "123-12-1234",
                FirstName = "John",
                LastName = "Smith",
                BirthDate = new DateTime(1950, 1, 1)
            };

            Assert.IsTrue(CustomerManager.Insert(customer));
        }

        [TestMethod]
        public void UpdateTest()
        {
            Customer customer = new Customer
            {
                ID = -1,
                SSN = "345-34-3456",
                FirstName = "Jane",
                LastName = "Doe",
                BirthDate = new DateTime(1960, 2, 2)
            };

            Assert.IsTrue(CustomerManager.Update(customer));
        }

        [TestMethod]
        public void DeleteTest()
        {
            Customer customer = new Customer
            {
                ID = -1,
                SSN = "345-34-3456",
                FirstName = "Jane",
                LastName = "Doe",
                BirthDate = new DateTime(1960, 2, 2)
            };

            Assert.IsTrue(CustomerManager.Delete(customer));
        }
    }
}
