﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.Banking.BL;
using JDF.Banking.BL.Models;
using System.Collections.Generic;

namespace JDF.Banking.BL.Test
{
    [TestClass]
    public class utDeposit
    {
        [TestMethod]
        public void PopulateTest()
        {
            List<Deposit> deposits = DepositManager.Populate(1);
            Assert.IsTrue(deposits.Count == 3);
        }

        [TestMethod]
        public void LoadTest()
        {
            List<Deposit> deposits = DepositManager.Load(1);
            Assert.AreEqual(3, deposits.Count);
        }
    }
}
