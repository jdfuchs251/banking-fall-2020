﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JDF.Banking.BL;
using JDF.Banking.BL.Models;
using System.Collections.Generic;

namespace JDF.Banking.BL.Test
{
    [TestClass]
    public class utWithdrawal
    {
        [TestMethod]
        public void PopulateTest()
        {
            List<Withdrawal> withdrawals = WithdrawalManager.Populate(1);
            Assert.IsTrue(withdrawals.Count == 3);
        }

        [TestMethod]
        public void LoadTest()
        {
            List<Withdrawal> withdrawals = WithdrawalManager.Load(1);
            Assert.AreEqual(3, withdrawals.Count);
        }
    }
}
