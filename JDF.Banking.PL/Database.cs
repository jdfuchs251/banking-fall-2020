﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JDF.Banking.PL
{
    public class Database
    {
        SqlConnection sqlConnection;

        /**********************************************************************************
        /* Name: Open
        /* Description: Opens a database connection using the connection string in app settings
        /* Returns: ConnectionState (should be open if successful)
        /*********************************************************************************/
        public ConnectionState Open()
        {
            try
            {
                sqlConnection = new SqlConnection(Properties.Settings.Default.DBConnectionString);
                sqlConnection.Open();
                return sqlConnection.State;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Close
        /* Description: Closes a database connection
        /* Returns: ConnectionState.Closed (as long as connection closed with no exceptions thrown)
        /*********************************************************************************/
        public ConnectionState Close()
        {
            try
            {
                sqlConnection.Close();
                sqlConnection = null;
                return ConnectionState.Closed; // Closed successfully - return Closed
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Select
        /* Description: Performs an SQL select query
        /* Parameters:
        /*   SqlCommand sqlCommand - Command to be executed
        /* Returns: DataTable containing query results
        /*********************************************************************************/
        public DataTable Select(SqlCommand sqlCommand)
        {
            try
            {
                Open();
                sqlCommand.Connection = sqlConnection;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                DataTable dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);
                Close();
                return dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Insert
        /* Description: Performs an SQL insert statement
        /* Parameters:
        /*   SqlCommand sqlCommand - Command to be executed
        /* Returns: Integer containing the number of rows inserted
        /*********************************************************************************/
        public int Insert(SqlCommand sqlCommand)
        {
            try
            {
                return ExecuteSQL(sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Update
        /* Description: Performs an SQL update statement
        /* Parameters:
        /*   SqlCommand sqlCommand - Command to be executed
        /* Returns: Integer containing the number of rows updated
        /*********************************************************************************/
        public int Update(SqlCommand sqlCommand)
        {
            try
            {
                return ExecuteSQL(sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Delete
        /* Description: Performs an SQL delete statement
        /* Parameters:
        /*   SqlCommand sqlCommand - Command to be executed
        /* Returns: Integer containing the number of rows deleted
        /*********************************************************************************/
        public int Delete(SqlCommand sqlCommand)
        {
            try
            {
                return ExecuteSQL(sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: ExecuteSQL
        /* Description: Executes an SQL command
        /* Parameters:
        /*   SqlCommand sqlCommand - Command to be executed
        /* Returns: Integer containing the number of rows affected
        /*********************************************************************************/
        private int ExecuteSQL(SqlCommand sqlCommand)
        {
            try
            {
                if (Open() == ConnectionState.Open)
                {
                    sqlCommand.Connection = sqlConnection;
                    int results = sqlCommand.ExecuteNonQuery();
                    Close();
                    return results;
                }
                else
                {
                    return 0; // No rows affected if connection wasn't opened
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
