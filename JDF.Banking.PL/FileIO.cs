﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JDF.Banking.PL
{
    public class FileIO
    {
        public FileIO() // Default constructor - set default location
        {
            Location = Environment.CurrentDirectory;
        }

        public FileIO(string location) // Custom constructor - set custom path
        {
            Location = location;
        }

        private string fileName;

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        private string location;

        public string Location
        {
            get { return location; }
            set { location = value; }
        }

        public string FilePath
        {
            get { return location + "\\" + fileName; }
        }

        /**********************************************************************************
        /* Name: Save
        /* Description: Saves data to a file
        /* Parameters:
        /*   string content - Contents to be saved
        /*********************************************************************************/
        public void Save(string content)
        {
            try
            {
                if (FilePath != string.Empty)
                {
                    StreamWriter streamWriter = File.AppendText(FilePath);
                    streamWriter.WriteLine(content);
                    streamWriter.Close();
                    streamWriter = null;
                }
                else
                {
                    throw new Exception("Missing file path");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Read
        /* Description: Reads data from a file
        /* Returns: File contents as string
        /*********************************************************************************/
        public string Read()
        {
            try
            {
                if (File.Exists(FilePath))
                {
                    StreamReader streamReader = new StreamReader(FilePath);
                    streamReader = File.OpenText(FilePath);
                    string content = streamReader.ReadToEnd();
                    streamReader.Close();
                    streamReader = null;
                    return content;
                }
                else
                {
                    throw new Exception(FilePath + " does not exist.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /**********************************************************************************
        /* Name: Deletes
        /* Description: Deles a file
        /*********************************************************************************/
        public void Delete()
        {
            if (File.Exists(FilePath))
            {
                File.Delete(FilePath);
            }
        }
    }
}
